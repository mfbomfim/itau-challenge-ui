import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SecurityService } from '@app/shared/services/security.service';
import { Router } from '@angular/router';
import { UserService } from '@app/shared/services/user.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.sass']
})
export class CreateUserComponent implements OnInit {
  userForm: FormGroup;
  erros: string;
  successMSG: string

  constructor(
    private service: UserService,
    private router: Router,
    private fb: FormBuilder,
  ) {
  }

  ngOnInit(): void {
    this.userForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      name: ['', Validators.compose([Validators.required,  Validators.minLength(5)])]
    });
  }

  save(event: Event) {
    event.preventDefault();
    this.successMSG = '';

    const success = () => this.successMSG = 'Cadastrado com sucesso';

    const fail = (response: HttpErrorResponse) => this.erros = response.error.message;

    const user = this.userForm.value;
    this.service.createUser(user, success, fail);
  }

}
