import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './pages/home/home.component';
import { SharedModule } from '@app/shared/shared.module';
import { MaterialModule } from '@app/core/material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FormExpenseComponent } from './pages/form-expense/form-expense.component';


@NgModule({
  declarations: [HomeComponent, FormExpenseComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  exports: [HomeComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [
    FormExpenseComponent
  ]
})
export class HomeModule { }
