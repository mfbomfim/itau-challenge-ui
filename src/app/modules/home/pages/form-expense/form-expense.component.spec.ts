import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormExpenseComponent } from './form-expense.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { By } from 'protractor';

describe('FormExpenseComponent', () => {
  let component: FormExpenseComponent;
  let fixture: ComponentFixture<FormExpenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormExpenseComponent ],
      imports: [
        ReactiveFormsModule,
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormExpenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should when run save() emit event to listener', () => {
    let expected;
    component.saveEmitter.subscribe(e => expected = e);
    const event = new MouseEvent('click');

    component.form.get('name').setValue('Teste');
    component.save(event);
    // const button = fixture.debugElement.nativeElement.query(By.className('custom-btn'));
    // fixture.debugElement.nativeElement.query(By.name('name')).value = 'Teste';

    // // name.value = 'Teste';

    // button.triggerEventHandler('click', {});
    // // fixture.detectChanges();
console.log(component.form.value);
    expect(expected).toEqual(component.form.value);
  });
});
