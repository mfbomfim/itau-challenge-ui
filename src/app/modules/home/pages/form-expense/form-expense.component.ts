import { Component, OnInit, EventEmitter } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-expense',
  templateUrl: './form-expense.component.html',
  styleUrls: ['./form-expense.component.sass']
})
export class FormExpenseComponent implements OnInit {

  form: FormGroup;

  // tslint:disable-next-line: new-parens
  saveEmitter = new EventEmitter;

  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      name: ['', Validators.required],
      group: ['', Validators.required],
      expenseAmount: ['', Validators.required]
    });
  }

  save(event: Event) {
    event.preventDefault();

    this.saveEmitter.emit(this.form.value);
  }
}
