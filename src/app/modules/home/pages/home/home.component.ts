import { Component, OnInit } from '@angular/core';
import { FormBuilder, NgForm } from '@angular/forms';
import { ExpenseService } from '@app/shared/services/expense.service';
import { Expense } from '@app/shared/models/expense';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { FormExpenseComponent } from '../form-expense/form-expense.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  expenses: Expense[];
  dataSource: Expense[];
  page = 1;
  pageSize = 10;
  displayedColumns: string[] = ['name', 'data', 'expenseAmount', 'group'];

  constructor(
    private service: ExpenseService,
    private fb: FormBuilder,
    private dialog: MatDialog,
  ) {
    this.findExpense();
  }

  ngOnInit(): void {
  }

  openModal() {
    const ref = this.dialog.open(FormExpenseComponent, {
      panelClass: 'default-modal-width',
      disableClose: true,
      data: {
        interface: this.save
      }
    });

    ref.componentInstance.saveEmitter.subscribe((expense) => {
      this.save(expense, ref);
    });
  }

  save(expense, dialogRef: MatDialogRef<any>) {
    const success = () => {
      this.findExpense();
      dialogRef.close();
    };
    expense.date = new Date().toISOString();

    this.service.save(expense, success);
  }

  findExpense() {
    const success = data => {
      this.expenses = data;
      this.startPaginator();
    };

    this.service.find(success);
  }

  filterExpense(form: NgForm) {
    const success = data => {
      this.expenses = data;
      this.startPaginator();
    };

    this.service.filter(form.value, success);
  }

  paginator(event) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;

    this.startPaginator();
  }

  startPaginator() {
    this.dataSource = this.expenses.slice((this.page - 1) * this.pageSize, ((this.page - 1) * this.pageSize) + this.pageSize);
  }
}
