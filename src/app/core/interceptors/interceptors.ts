import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { HttpIpmlInterceptor } from '@app/core/interceptors/http-impl.interceptor';

export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: HttpIpmlInterceptor, multi: true }
];
