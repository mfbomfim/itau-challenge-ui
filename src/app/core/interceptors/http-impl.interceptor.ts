import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';

@Injectable()
export class HttpIpmlInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = localStorage.getItem('token');
    let headers = req.headers;
    if (token) {
      headers = headers.set('X-XSRF-TOKEN', localStorage.getItem('token'));
    }
    const securityReq = req.clone({
      headers
    });
    return next.handle(securityReq);
  }
}
