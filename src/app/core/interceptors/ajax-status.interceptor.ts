import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class AjaxInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const reportProgressReq = req.clone({
      reportProgress: true
    });

    return next.handle(reportProgressReq).pipe(
      tap(event => {
        if (event instanceof HttpResponse) {
        }
      })
    );
  }
}
