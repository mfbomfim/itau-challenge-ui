import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SecurityService } from '@app/shared/services/security.service';

@Injectable()
export class LoginGuard implements CanActivate {
  constructor(
    private session: SecurityService,
    private router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.session.isAuth()) {
      return true;
    }
    this.router.navigate(['/home']);
    return false;
  }
}
