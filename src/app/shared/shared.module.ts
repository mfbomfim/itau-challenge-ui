import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { MaterialModule } from '@app/core/material/material.module';
import { LogoComponent } from './components/logo/logo.component';
import { LocaleDatePipe } from './pipes/locale-date.pipe';
import { LocaleTimePipe } from './pipes/locale-time.pipe';
import { MAT_DATE_LOCALE } from '@angular/material/core';

@NgModule({
  declarations: [HeaderComponent, LogoComponent, LocaleDatePipe, LocaleTimePipe],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [HeaderComponent, LogoComponent, LocaleTimePipe, LocaleDatePipe],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: 'pt-BR' }]
})
export class SharedModule { }
