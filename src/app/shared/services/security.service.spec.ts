import { TestBed, getTestBed } from '@angular/core/testing';
import { SecurityService } from './security.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';


describe('SecurityService', () => {
  let service: SecurityService;
  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [SecurityService]
    });
    injector = getTestBed();
    service = injector.get(SecurityService);
    httpMock = injector.get(HttpTestingController);
  });
  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
