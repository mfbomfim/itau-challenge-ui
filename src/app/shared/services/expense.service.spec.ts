import { TestBed, getTestBed } from '@angular/core/testing';

import { ExpenseService } from './expense.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('ExpenseService', () => {
  let service: ExpenseService;
  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ExpenseService]
    });
    injector = getTestBed();
    service = injector.get(ExpenseService);
    httpMock = injector.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
