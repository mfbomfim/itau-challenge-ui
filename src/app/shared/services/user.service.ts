import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '@env/environment';
import { CreateUser } from '@app/shared/models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private URL: string = environment.urlApi;
  private _TOKEN_KEY = environment.token_key;
  header = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(
    private http: HttpClient,
  ) { }

  createUser(user: CreateUser, success, fail) {
    return this.http.post(`${this.URL}/users`, user, { headers: this.header }).subscribe(success, fail);
  }
}
