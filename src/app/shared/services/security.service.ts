import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '@env/environment';
import { User } from '@app/shared/models/user';

@Injectable()
export class SecurityService {

  static emitterAuth = new EventEmitter<boolean>();

  private URL: string = environment.urlApi;
  private _TOKEN_KEY = environment.token_key;
  header = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(
    private router: Router,
    private http: HttpClient,
  ) { }

  logIn(credencial, success) {
    return this.http.post<Auth>(`${this.URL}/auth`, credencial, { headers: this.header }).subscribe((auth) => {
      localStorage.setItem(this._TOKEN_KEY, auth.token.toString());
      SecurityService.emitterAuth.emit(true);
      success();
    });
  }

  logout() {
    localStorage.removeItem(this._TOKEN_KEY);
    SecurityService.emitterAuth.emit(false);
    this.router.navigate(['/login']);
  }

  isAuth(): boolean {
    const token = localStorage.getItem(this._TOKEN_KEY);

    if (token && token.length) {
      SecurityService.emitterAuth.emit(true);
      return true;
    }
    SecurityService.emitterAuth.emit(false);
    return false;
  }

  hasError401(response, onFail) {
    if (response.status === 401) {
      this.logout();
    } else {
      onFail();
    }
  }
}

export class Auth {
  token: string;
  user: User;
}
