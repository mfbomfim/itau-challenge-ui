import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '@env/environment';
import { Expense } from '@app/shared/models/expense';

@Injectable()
export class ExpenseService {

  private URL: string = environment.urlApi;
  private _TOKEN_KEY = environment.token_key;
  header = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(
    private http: HttpClient,
  ) { }

  save(expense, success) {
    const token: string = localStorage.getItem(this._TOKEN_KEY);
    this.http.post(`${this.URL}/expenses`, expense, {
      headers: {
        'x-access-token': token,
      }
    }).subscribe(success, (e) => {
      console.log(e);
    });
  }

  update() {}

  find(success) {
    const token: string = localStorage.getItem(this._TOKEN_KEY);
    this.http.get(`${this.URL}/expenses`, {
      headers: {
        'x-access-token': token,
      }
    }).subscribe(success);
  }

  filter(filter: any, success) {
    Object.keys(filter).forEach(key => {
      if (!filter[key]) { delete filter[key]; }
    });

    const token: string = localStorage.getItem(this._TOKEN_KEY);
    this.http.get(`${this.URL}/expenses/filter`, {
      headers: {
        'x-access-token': token,
      },
      params: filter
    }).subscribe(success);
  }

  delete() {}
}
