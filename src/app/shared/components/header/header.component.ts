import { Component, OnInit } from '@angular/core';
import { SecurityService } from '@app/shared/services/security.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {
  isAuth: boolean;

  constructor(
    private securityService: SecurityService
  ) { }

  ngOnInit(): void {
    SecurityService.emitterAuth.subscribe((auth) => {
      this.isAuth = auth;
    });
  }

  logout = () => this.securityService.logout();
}
