export class User {
  name: string;
  email: string;
}

export class CreateUser extends User {
  password: string;
}
