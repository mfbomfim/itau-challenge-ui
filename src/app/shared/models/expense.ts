export class Expense {
  id: string;
  date: Date;
  name: string;
  description: string;
  group: string;
}
